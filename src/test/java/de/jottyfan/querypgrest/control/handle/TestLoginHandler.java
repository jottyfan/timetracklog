package de.jottyfan.querypgrest.control.handle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author jotty
 *
 */
public class TestLoginHandler {

	@Test
	public void testApply() throws Exception {
		Object o = new LoginHandler().apply(null);
		assertNotNull(o);
		assertEquals("<html>\n  <body>\n    <h1>Welcome, user</h1>\n  </body>\n</html>\n", o);
	}
}
