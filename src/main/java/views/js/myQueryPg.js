myQueryPg = {
		
	run : function(querypg) {
		querypg.run();
	},
	
	doCallRest : function(querypg, page) {
		querypg.callRest(page);
		this.run(querypg);
	}
};
