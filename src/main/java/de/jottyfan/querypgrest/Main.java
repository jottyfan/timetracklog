package de.jottyfan.querypgrest;

import de.jottyfan.querypgrest.control.Pages;
import io.jooby.Jooby;
import io.jooby.ServerOptions;
import io.jooby.freemarker.FreemarkerModule;

/**
 * 
 * @author jotty
 *
 */
public class Main {
	public static void main(String... args) {
		Jooby.runApp(args, app -> {
			// TODO: find out how to use application.conf instead
			ServerOptions options = app.getServerOptions();
			if (options == null) {
				options = new ServerOptions();
				app.setServerOptions(options);
			}
			options.setPort(8888);

			app.assets("bootstrap.min.css", "/views/css/bootstrap.min.css");
			app.assets("style.css", "/views/css/style.css");
			app.assets("bootstrap.min.js", "/views/js/bootstrap.min.js");
			app.assets("jquery.js", "/views/js/jquery-3.3.1.slim.min.js");
			app.assets("myQueryPg.js", "/views/js/myQueryPg.js");
			app.assets("popper.min.js", "/views/js/popper.min.js");
			app.assets("querypg.js", "/views/js/querypg.js");
			
			app.install(new FreemarkerModule());
			
			new Pages(app).register();
		});
	}
}
