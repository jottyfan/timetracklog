package de.jottyfan.querypgrest.control.html.bs;

import de.jottyfan.querypgrest.control.html.Div;
import de.jottyfan.querypgrest.control.html.HtmlTag;

/**
 * 
 * @author jotty
 *
 */
public class CardBody extends Div {
	public CardBody() {
		super();
		set("class", "card-body");
	}

	@Override
	public CardBody css(String key, String value) {
		super.css(key, value);
		return this;
	}

	@Override
	public CardBody add(HtmlTag child) {
		super.add(child);
		return this;
	}
}
