package de.jottyfan.querypgrest.control.html.bs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.jottyfan.querypgrest.control.html.HtmlTag;

/**
 * 
 * @author jotty
 *
 */
public class Input extends HtmlTag {

	private final List<HtmlTag> children;
	private final Map<String, String> attributes;

	public enum InputType {
		TEXT("text"), PASSWORD("password");
		private final String value;

		private InputType(String value) {
			this.value = value;
		}

		public final String get() {
			return value;
		}
	}

	public Input(InputType inputType) {
		this.children = new ArrayList<>();
		this.attributes = new HashMap<>();
		set("type", inputType.get());
		set("class", "form-control");
	}

	@Override
	public String getName() {
		return "input";
	}

	@Override
	public List<HtmlTag> getChildren() {
		return children;
	}

	@Override
	public Map<String, String> getAttributes() {
		return attributes;
	}

	public Input set(String key, String value) {
		attributes.put(key, value);
		return this;
	}

	public Input add(HtmlTag child) {
		children.add(child);
		return this;
	}
}
