package de.jottyfan.querypgrest.control.html.bs;

import de.jottyfan.querypgrest.control.html.Div;
import de.jottyfan.querypgrest.control.html.HtmlTag;

/**
 * 
 * @author jotty
 *
 */
public class Container extends Div {
	public Container() {
		super();
		css("class", "container");
	}

	@Override
	public Container add(HtmlTag child) {
		getChildren().add(child);
		return this;
	}
}
