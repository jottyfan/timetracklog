package de.jottyfan.querypgrest.control.html.bs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.jottyfan.querypgrest.control.html.HtmlTag;

/**
 * 
 * @author jotty
 *
 */
public class Button extends HtmlTag {

	private final List<HtmlTag> children;
	private final Map<String, String> attributes;

	public enum ButtonType {
		PRIMARY("btn btn-primary"), SUCCESS("btn btn-success"), DANGER("btn btn-danger"), WARNING("btn btn-warning");
		private final String value;

		private ButtonType(String value) {
			this.value = value;
		}

		public final String get() {
			return value;
		}
	}

	public Button(ButtonType buttonType) {
		this.children = new ArrayList<>();
		this.attributes = new HashMap<>();
		set("class", buttonType.get());
	}

	@Override
	public String getName() {
		return "button";
	}

	@Override
	public List<HtmlTag> getChildren() {
		return children;
	}

	@Override
	public Map<String, String> getAttributes() {
		return attributes;
	}

	public Button set(String key, String value) {
		attributes.put(key, value);
		return this;
	}

	public Button add(HtmlTag child) {
		children.add(child);
		return this;
	}
}
