package de.jottyfan.querypgrest.control.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author jotty
 *
 */
public class Div extends HtmlTag {

	private final List<HtmlTag> children;
	private final Map<String, String> attributes;

	public Div() {
		this.children = new ArrayList<>();
		this.attributes = new HashMap<>();
	}

	@Override
	public String getName() {
		return "div";
	}

	@Override
	public List<HtmlTag> getChildren() {
		return children;
	}

	@Override
	public Map<String, String> getAttributes() {
		return attributes;
	}

	/**
	 * add a html child
	 * 
	 * @param child
	 *          the child
	 * @return this
	 */
	public Div add(HtmlTag child) {
		children.add(child);
		return this;
	}
	
	/**
	 * set attribute
	 * 
	 * @param key the key
	 * @param value the value
	 * @return this
	 */
	public Div set(String key, String value) {
		attributes.put(key, value);
		return this;
	}

	/**
	 * add style
	 * 
	 * @param key
	 *          the css key
	 * @param value
	 *          the css value
	 * @return this
	 */
	public Div css(String key, String value) {
		super.addAttributesToStyle(key, value);
		return this;
	}
}
