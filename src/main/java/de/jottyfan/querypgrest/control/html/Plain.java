package de.jottyfan.querypgrest.control.html;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author jotty
 *
 */
public class Plain extends HtmlTag {

	private final String value;

	public Plain(String value) {
		this.value = value;
	}

	@Override
	public String toHtml() {
		return value; // TODO: escape
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public List<HtmlTag> getChildren() {
		return null;
	}

	@Override
	public Map<String, String> getAttributes() {
		return null;
	}
}
