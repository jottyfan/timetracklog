package de.jottyfan.querypgrest.control.html.bs;

import de.jottyfan.querypgrest.control.html.Div;
import de.jottyfan.querypgrest.control.html.HtmlTag;

/**
 * 
 * @author jotty
 *
 */
public class Col extends Div {

	public enum ColType {
		SM("col-sm"), MD_AUTO("col-md-auto");
		private final String value;

		private ColType(String value) {
			this.value = value;
		}

		public final String get() {
			return this.value;
		}
	}

	public Col(ColType colType) {
		super();
		set("class", colType.get());
	}

	public Col(String classAttribute) {
		super();
		set("class", classAttribute);
	}

	public Col add(HtmlTag child) {
		getChildren().add(child);
		return this;
	}
}
