package de.jottyfan.querypgrest.control.html.bs;

import de.jottyfan.querypgrest.control.html.Div;
import de.jottyfan.querypgrest.control.html.HtmlTag;

/**
 * 
 * @author jotty
 *
 */
public class CardTitle extends Div {
	public CardTitle() {
		super();
		set("class", "card-title");
	}

	@Override
	public CardTitle css(String key, String value) {
		super.css(key, value);
		return this;
	}

	@Override
	public CardTitle add(HtmlTag child) {
		super.add(child);
		return this;
	}
}
