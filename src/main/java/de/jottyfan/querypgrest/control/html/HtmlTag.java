package de.jottyfan.querypgrest.control.html;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author jotty
 *
 */
public abstract class HtmlTag {

	/**
	 * get the name of the html tag
	 * 
	 * @return the tag name
	 */
	public abstract String getName();

	/**
	 * get the children html tags
	 * 
	 * @return a list of children html tags
	 */
	public abstract List<HtmlTag> getChildren();

	/**
	 * get the tag attributes
	 * 
	 * @return a map of tag attributes
	 */
	public abstract Map<String, String> getAttributes();

	/**
	 * get the html code
	 * 
	 * @return the html code
	 */
	public String toHtml() {
		StringBuilder buf = new StringBuilder("<");
		buf.append(getName());
		if (getAttributes() != null) {
			for (Entry<String, String> entry : getAttributes().entrySet()) {
				buf.append(" ").append(entry.getKey()).append("=\\\"").append(entry.getValue()).append("\\\"");
			}
		}
		if (getChildren().size() > 0) {
			buf.append(">\\\n");
			for (HtmlTag child : getChildren()) {
				buf.append(child.toHtml()).append("\\\n");
			}
			buf.append("</").append(getName()).append(">\\\n");
		} else {
			buf.append(" />\\\n");
		}
		return buf.toString();
	}

	/**
	 * add style attribute
	 * 
	 * @param key
	 *          the css key
	 * @param value
	 *          the css value
	 */
	public void addAttributesToStyle(String key, String value) {
		String values = getAttributes().get("style");
		StringBuilder buf = new StringBuilder(values == null ? "" : values);
		buf.append(key).append(": ").append(value).append("; ");
		getAttributes().put("style", buf.toString());
	}
}
