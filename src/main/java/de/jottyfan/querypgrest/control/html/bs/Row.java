package de.jottyfan.querypgrest.control.html.bs;

import de.jottyfan.querypgrest.control.html.Div;
import de.jottyfan.querypgrest.control.html.HtmlTag;

/**
 * 
 * @author jotty
 *
 */
public class Row extends Div {
	public Row() {
		super();
		set("class", "row");
	}

	public Row(String classAttribute) {
		super();
		set("class", classAttribute);
	}
	
	@Override
	public Row add(HtmlTag child) {
		getChildren().add(child);
		return this;
	}
}
