package de.jottyfan.querypgrest.control.html.bs;

import de.jottyfan.querypgrest.control.html.Div;
import de.jottyfan.querypgrest.control.html.HtmlTag;

/**
 * 
 * @author jotty
 *
 */
public class Card extends Div {
	public Card() {
		super();
		set("class", "card");
	}

	@Override
	public Card css(String key, String value) {
		super.css(key, value);
		return this;
	}
	
	@Override
	public Card add(HtmlTag child) {
		getChildren().add(child);
		return this;
	}
}
