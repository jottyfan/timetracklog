package de.jottyfan.querypgrest.control.handle;

import de.jottyfan.querypgrest.control.html.Div;
import de.jottyfan.querypgrest.control.html.Plain;
import de.jottyfan.querypgrest.control.html.bs.Button;
import de.jottyfan.querypgrest.control.html.bs.Button.ButtonType;
import de.jottyfan.querypgrest.control.html.bs.Card;
import de.jottyfan.querypgrest.control.html.bs.CardBody;
import de.jottyfan.querypgrest.control.html.bs.CardTitle;
import de.jottyfan.querypgrest.control.html.bs.Col;
import de.jottyfan.querypgrest.control.html.bs.Col.ColType;
import de.jottyfan.querypgrest.control.html.bs.Container;
import de.jottyfan.querypgrest.control.html.bs.Input;
import de.jottyfan.querypgrest.control.html.bs.Input.InputType;
import de.jottyfan.querypgrest.control.html.bs.Row;
import io.jooby.Context;
import io.jooby.ModelAndView;
import io.jooby.Route.Handler;

/**
 * 
 * @author jotty
 *
 */
public class LoginHandler implements Handler {
	private static final long serialVersionUID = 1L;

	private Card loginPanel() {
		CardTitle title = new CardTitle().css("text-align", "center").css("background-color", "rgb(0,123,255)")
				.css("color", "white").add(new Plain("please log in"));
		Row usernameRow = new Row().add(new Col(ColType.SM).add(new Plain("username")));
		usernameRow.add(new Col(ColType.SM).add(new Input(InputType.TEXT).set("value", "").set("placeholder", "username")
				.set("onchange", "querypg.setParam('username', this.value)")));
		Row passwordRow = new Row().add(new Col(ColType.SM).add(new Plain("password")));
		passwordRow.add(new Col(ColType.SM).add(new Input(InputType.PASSWORD).set("value", "")
				.set("placeholder", "username").set("onchange", "querypg.setParam('password', this.value)")));
		return new Card().css("max-width", "400px").add(title)
				.add(new CardBody().add(new Container().add(usernameRow).add(passwordRow)).add(new Button(ButtonType.PRIMARY)
						.set("onclick", "myQueryPg.doCallRest(querypg, 'login')").add(new Plain("submit"))));
	}

	private Div getLoginPage() {
		return new Container()
				.add(new Row("row justify-content-md-center").add(new Col(ColType.MD_AUTO).add(loginPanel())));
	}

	private Div getFoot() {
		return new Div().css("background-color", "#2073b9").css("color", "white").css("width", "100%")
				.add(new Plain("querypg"));
	}

	@Override
	public Object apply(Context ctx) throws Exception {
		ModelAndView mav = new ModelAndView("template.html");
		mav.put("user", "Hanswurst");
		mav.put("body", getLoginPage().toHtml());
		mav.put("foot", getFoot().toHtml());
		return mav;
	}
}
