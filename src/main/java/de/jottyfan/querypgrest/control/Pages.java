package de.jottyfan.querypgrest.control;

import de.jottyfan.querypgrest.control.handle.LoginHandler;
import io.jooby.Jooby;

/**
 * 
 * @author jotty
 *
 */
public class Pages {

	private final Jooby app;
	
	public Pages(Jooby app) {
		this.app = app;
	}

	public void register() {
		app.get("login", ctx -> new LoginHandler().apply(ctx));
		app.get("logout", ctx -> new LoginHandler().apply(ctx));
	}
}
